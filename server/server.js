const {ApolloServer} = require('apollo-server'); // Tag function to parse the schema
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb+srv://admin:00000@cluster0.8xrr4.mongodb.net/slidb?retryWrites=true&w=majority";
const { gql } = require('apollo-server-express');

const typeDefs = gql`
  schema {
    query: Query   
	mutation: Mutation
  }
  

  type Query {
	  createmock(email: String!, status: String!, date: String!): User
    
  } 

   
  extend type Query {
	login(email: String!, password: String!): User
    }
	
	type Mutation {
		  createaccount(email: String!, name: String!, surname: String!, password: String!): User
	}
	
	
    type User {
        name: String!
        surname: String! 
        email: String! 
        password: String! 
        status: String! 
		response: String! 
		date: String! 
    }
`;

const resolvers = {
	 
  Query: {
    createmock: (_, {email,  status, date}) => createMock(email, status, date)
  },
  Mutation: {
    createaccount: (_, {email, name, surname, password}) => createAccountDemo(email, name, surname, password)
  },
  Query: {
    login: (_, {email,  password}) => logIn(email, password)
  }
  
}

function helloTest( name) {
	console.log(" On " + name); 
	return {response: "Hello Boo"};
}	

function createMock(email,  status, date) {	

	var mockdata = {
		email: email,
		status: status,
		date: date
	}
	
	MongoClient.connect(url, function(err, db) {		
	  if (err) throw err;
	  var dbo = db.db("slidb"); 
	  
	  dbo.collection("capestatus").insertOne(mockdata, function(err1, res1) {
		if (err1) {
		  console.log(err1);
		  db.close();
		  return {response: "something went wrong creating mock"}; 
		} else {
			
		  if (res1) {	
			  console.log("1 document inserted");
			  db.close();
			  console.log(email + "  " + email + "  " + status );
			 return {response: "mock created"};
		  }
		  
		}		
	  });	  
	});	
}

function logIn(email, password) {
	
	MongoClient.connect(url, function(err, db) {		
	  if (err) throw err;
	  var dbo = db.db("slidb"); 
	  
	  dbo.collection("capecompany").findOne({'email': email}, function(err1, res1) {
		if (err1) {
		  console.log(err1);
		  db.close();
		  return {response: "something went wrong creating mock"}; 
		} else {
			
		  if (res1) {	
			  console.log("1 document found");
			  db.close(); 
			 return {response: "logged in successfully"};
		  }		  
		}		
	  });	 
	  
	});	
}

function createAccountDemo(email, name, surname, password) {
	
	console.log(" createAccountDemo ");
	var accdata = {
		name: name,
		email: email,
		surname: surname,
		password: password
	}	
	
	MongoClient.connect(url, function(err, db) {		
	  if (err) throw err;
	  var dbo = db.db("slidb");
	  
	  dbo.collection("capecompany").insertOne(accdata, function(err1, res1) {
		if (err1) { 
		  console.log(err1);
		  db.close();
		  return {response : "TEST"};
		} else {
			
		  if (res1) {	
			  console.log("1 document inserted");
			  db.close();
			  console.log(email + "  " + name + "  " + surname + "  " + password);
			  return {response : "TEST"};
		  }
		}		
	  });	  
	});	 
}

function getAccountData(email,  password) {
	console.log(" getAccountData "); 
	return {response: "Object"};
} 

const server = new ApolloServer({typeDefs, resolvers});
server.listen({port: 9000})
  .then(({url}) => console.log(`Server running at ${url}`));
